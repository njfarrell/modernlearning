package com.njfarrell.mobile.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.njfarrell.mobile.fragments.CardDecksListFragment;
import com.njfarrell.mobile.fragments.CardDecksListFragment.CardDeck;
import com.njfarrell.mobile.services.ContentService;

public class CardDeckActivity extends FragmentActivity implements
		OnItemSelectedListener {

	private static final String TAG = CardDeckActivity.class.getSimpleName();

	private ResponseReceiver receiver;

	private String api_do = null;
	private JSONObject api_with = null;
	private JSONObject response = null;

	private SharedPreferences config_prefs;
    private SharedPreferences prefs;

	private SharedPreferences.Editor editor;

	private static final String APP_PREFERENCES = "app_preferences";
	private static final String NEW_DEVICE_INFO = "new_app_info";

	private ArrayAdapter<String> adapter;
	private Spinner spinner;
	private List<CardDeck> deckList = null;

    protected long maxAge = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_card_deck_activity);
		adapter = new ArrayAdapter<String>(this, R.layout.deckset_selector);
		adapter.setDropDownViewResource(R.layout.dropdown_deckset);
		spinner = (Spinner) this.findViewById(R.id.deckSet);
		spinner.setOnItemSelectedListener(this);
		spinner.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
		return super.onOptionsItemSelected(item);
	}

    boolean firstRun = true;
	private void initializeApp() {
		config_prefs = getSharedPreferences(APP_PREFERENCES, 0);
        prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		editor = config_prefs.edit();
		if (config_prefs.contains(NEW_DEVICE_INFO)) {
			api_do = "get_deckgroups";
			startContentService(true);
		} else {
            if(firstRun){
			api_do = "new_install";
			api_with = getDeviceInfo();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            alertDialogBuilder.setTitle("Welcome");

            // set dialog message
            alertDialogBuilder
                    .setMessage("To save you data and improve your experience, this application will download updates from the Internet only when you need them. "+
                            "Once you view a card side for the first time, we won't download it again. If you would like to use a card deck off-line, simply flip "+
                            "through the cards when you're on wireless, and they'll be available after that even when you don't have data. You can further control "+
                            "how often we update the decks in the application settings.")
                    .setCancelable(false)
                    .setPositiveButton("Got It!",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            dialog.dismiss();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            }
            firstRun = false;
            startContentService(false);
		}

	}

	private JSONObject getDeviceInfo() {
		JSONObject deviceInfo = new JSONObject();
		try {
			deviceInfo.put("device", Build.MODEL);
			deviceInfo.put("manufacturer", Build.MANUFACTURER);
			deviceInfo.put("android", android.os.Build.VERSION.SDK);

			TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

			String tmDevice = tm.getDeviceId();
			String androidId = Secure.getString(getContentResolver(),
					Secure.ANDROID_ID);
			String serial = null;
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO)
				serial = Build.SERIAL;
			if (androidId != null)
				deviceInfo.put("uid", androidId);
			else if (serial != null)
				deviceInfo.put("uid", serial);
			else if (tmDevice != null)
				deviceInfo.put("uid", tmDevice);
			deviceInfo.put("carrier", tm.getNetworkOperatorName());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return deviceInfo;
	}

	public void startContentService(boolean cache) {
		Intent i = new Intent(this, ContentService.class);
		maxAge = Integer.parseInt(prefs.getString("autorefresh_frequency", "172800"));
		if (api_do != null) {
			i.putExtra("do", api_do);
		}
		if (api_with != null) {
			i.putExtra("with", api_with.toString());
		}
		i.putExtra("cache", cache);
		i.putExtra("age", maxAge);
		i.putExtra("url", ContentService.URL);

		Log.d(TAG, "Starting ContentService:");
		this.startService(i);
	}

	@Override
	public void onResume() {
        Log.d("CardDeckActivity", "OnResume");
        initializeApp();
		IntentFilter filter = new IntentFilter(ResponseReceiver.CARD_DECK_RESPONSE);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ResponseReceiver();
		registerReceiver(receiver, filter);
		super.onResume();
	}

	@Override
	public void onPause() {
		unregisterReceiver(receiver);
		super.onPause();
	}

	private void setupDecksetDropdown(JSONObject response) {
		JSONArray dataArray = new JSONArray();
		try {
            adapter.clear();
            adapter.add("All Decks");
			dataArray = response.getJSONArray("data");
			for (int i = 0; i < dataArray.length(); i++) {
				JSONObject group = new JSONObject(dataArray.get(i).toString());
				String deckGroup = group.optString("deck_group");
				adapter.add(deckGroup);
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void createDeckList(JSONObject response) {

		CardDecksListFragment cardDeck = new CardDecksListFragment();
		deckList = new ArrayList<CardDeck>();
		JSONArray dataArray = new JSONArray();
		try {
			dataArray = response.getJSONArray("data");
			for (int i = 0; i < dataArray.length(); i++) {
				final JSONObject decks = new JSONObject(dataArray.get(i).toString());
				CardDeck deck = cardDeck.new CardDeck(
						decks.getString("deck_title"),
                        decks.getString("description"),
						decks.getString("cover"),
						decks.getString("deck_group"),
						decks.getInt("id"));
				deckList.add(deck);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		launchDeckFragment();
	}

	private void launchDeckFragment() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		CardDecksListFragment cardDeck = new CardDecksListFragment();
		String deckGroup = (String) spinner.getSelectedItem();
		Bundle args = new Bundle();

		Log.d(TAG, "deckGroup=" + deckGroup);
		if (deckList != null) {
			int i = 0;
			for (CardDeck d : deckList) {
				if (d.getGroup().equals(deckGroup)
						|| deckGroup.equals("All Decks")) {
					args.putParcelable("deck" + i, d);
					i++;
				}
			}
		}

		cardDeck.setArguments(args);
		ft.replace(R.id.flash_card_deck_fragment, cardDeck);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		ft.commit();
	}

	public class ResponseReceiver extends BroadcastReceiver {
		public static final String CARD_DECK_RESPONSE = "com.njfarrell.mobile.intent.action.cardDeck";

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = new Bundle();
			b = intent.getExtras();
			if (b.containsKey(ContentService.RESPONSE_JSON)) {
				try {
					Log.d(TAG, b.getString(ContentService.RESPONSE_JSON));
					response = new JSONObject(
							b.getString(ContentService.RESPONSE_JSON));
					if (response.has("success")) {
						boolean success = (Boolean) response.get("success");
						if (success && api_do.equals("new_install")) {
							editor.putString(NEW_DEVICE_INFO, api_with.toString());
							editor.commit();
							api_with = null;
							api_do = "get_deckgroups";
							startContentService(true);
						} else if (success && api_do.equals("get_deckgroups")) {
							setupDecksetDropdown(response);
							api_do = "get_decks";
							startContentService(true);
						} else if (success && api_do.equals("get_decks")) {
							createDeckList(response);
						} else {
	                        createDeckList(response);
	                    }
					} else {
//					Toast.makeText(CardDeckActivity.this,
//							response.getString("message"), Toast.LENGTH_SHORT)
//							.show();
					}
				} catch (JSONException e) {
					// Dont handle exception
				}
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View spinnerView,
			int position, long arg3) {
		launchDeckFragment();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}
