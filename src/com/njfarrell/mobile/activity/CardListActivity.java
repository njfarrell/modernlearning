package com.njfarrell.mobile.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.njfarrell.mobile.fragments.FlashCard;
import com.njfarrell.mobile.services.ContentService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nate on 6/8/13.
 */
@SuppressLint("NewApi")
public class CardListActivity extends FragmentActivity {
	private static final String TAG = CardListActivity.class.getSimpleName();

	private ArrayList<Card> questionCards = new ArrayList<Card>();
	private ArrayList<Card> answerCards = new ArrayList<Card>();

	private boolean isFlipped;
	private static boolean reverseMode = false;

	private ResponseReceiver receiver;

	private int currentPosition = 0;
	private int finalPosition = 0;
	private int deckId;

	private String api_do = null;
	private JSONObject api_with = null;
	private JSONObject response = null;
    private long maxAge = 0;

    public static RelativeLayout leftEdge = null;
    public static RelativeLayout rightEdge = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_flash_card);
		
		ActionBar actionBar = this.getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		deckId = getIntent().getIntExtra("id", -1);

		api_do = "get_cards";
		api_with = new JSONObject();
		try {
			api_with.put("deck", deckId);
		} catch (JSONException e) {
			e.printStackTrace();
		}


		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		reverseMode = prefs.getBoolean("reverse_mode", false);
        Calendar cal = Calendar.getInstance();
        long currentTime = cal.getTimeInMillis() / 1000;
        maxAge = currentTime - Integer.parseInt(prefs.getString("autorefresh_frequency", "172800"));
        Log.d("CardListActivity", "set maxAge:"+ maxAge);

        startContentService(true, maxAge);

		isFlipped = reverseMode;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.flashcard_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home){
			this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
			this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			return true;
		}
		if (item.getItemId() == R.id.Shuffle) {
			long seed = System.nanoTime();
			Collections.shuffle(questionCards, new Random(seed));
			Collections.shuffle(answerCards, new Random(seed));

			displayCard();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
        leftEdge = (RelativeLayout) findViewById(R.id.edge_left);
        rightEdge = (RelativeLayout) findViewById(R.id.edge_right);
		return super.onCreateView(name, context, attrs);
	}

	@Override
	public void onResume() {
		IntentFilter filter = new IntentFilter(ResponseReceiver.CARD_LIST_RESPONSE);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ResponseReceiver();
		registerReceiver(receiver, filter);

		super.onResume();
	}

	@Override
	public void onPause() {
		unregisterReceiver(receiver);

		super.onPause();
	}

	public void startContentService(boolean cache, long maxAge) {
		Intent i = new Intent(this, ContentService.class);
		if (api_do != null) {
			i.putExtra("do", api_do);
		}
		if (api_with != null) {
			i.putExtra("with", api_with.toString());
		}
		i.putExtra("cache", cache);
		i.putExtra("url", ContentService.URL);
        i.putExtra("age", maxAge);

		Log.d(TAG, "Starting ContentService:");
		this.startService(i);
	}

	public void onFlashCardClick(int index) {
		isFlipped = !isFlipped;
		displayCard();
	}

    public void setLeftEdge(int visibility){
        leftEdge.setVisibility(visibility);
    }

    public void setRightEdge(int visibility){
        rightEdge.setVisibility(visibility);
    }

	public void onFlashCardSwipe(String direction) {
		if (direction.equals("Left")) {
			if (currentPosition > 0) {
				isFlipped = reverseMode;
				currentPosition--;
				displayCard();
			} else {
                setLeftEdge(View.VISIBLE);

                new CountDownTimer(300,100) {

                    @Override
                    public void onTick(long l) {
                    }

                    @Override
                    public void onFinish() {
                        setLeftEdge(View.GONE);
                    }
                }.start();
            }
		} else if (direction.equals("Right")) {
			if (currentPosition < finalPosition) {
				isFlipped = reverseMode;
				currentPosition++;
				displayCard();
			} else {
                setRightEdge(View.VISIBLE);

                new CountDownTimer(300,100) {

                    @Override
                    public void onTick(long l) {
                    }

                    @Override
                    public void onFinish() {
                        setRightEdge(View.GONE);
                    }
                }.start();
			}
		}
	}

	public void onPreviousClick(View v) {
		if (currentPosition > 0) {
			isFlipped = reverseMode;
			currentPosition--;
			displayCard();
		} else {
            setLeftEdge(View.VISIBLE);

            new CountDownTimer(300,100) {

                @Override
                public void onTick(long l) {
                }

                @Override
                public void onFinish() {
                    setLeftEdge(View.GONE);
                }
            }.start();
        }
	}

	public void onFlipClick(View v) {
		isFlipped = !isFlipped;
		displayCard();
		Log.d(TAG, "flip was clicked");
	}

	public void onNextClick(View v) {
		if (currentPosition < finalPosition) {
			isFlipped = reverseMode;
			currentPosition++;
			displayCard();
		} else {
            setRightEdge(View.VISIBLE);

            new CountDownTimer(300,100) {

                @Override
                public void onTick(long l) {
                }

                @Override
                public void onFinish() {
                    setRightEdge(View.GONE);
                }
            }.start();
        }
	}

	private void displayCard() {
		Bundle args = new Bundle();
		ArrayList<Card> cards = new ArrayList<Card>();
		if (isFlipped) {
			cards.addAll(answerCards);
		} else {
			cards.addAll(questionCards);
		}
		finalPosition = cards.size() -1;
		args.putInt("pageNumber", currentPosition);
		args.putInt("maxNumber", finalPosition);
		if (cards != null && cards.size() > 0) {
			args.putString("title", cards.get(currentPosition).getTitle());
			args.putString("imageId", cards.get(currentPosition).getImageId());
			args.putString("description", cards.get(currentPosition)
					.getDescription());
		}

		FlashCard flashCardFragment = new FlashCard();
		flashCardFragment.setArguments(args);

		getSupportFragmentManager().beginTransaction()
				.replace(R.id.card_view, flashCardFragment).commit();
	}

	private void generateFlashCard(JSONObject response) {
		JSONArray flashCardArray = new JSONArray();
		try {
			flashCardArray = response.getJSONArray("data");
			for (int i = 0; i < flashCardArray.length(); i++) {
				JSONObject flashCard = new JSONObject(flashCardArray.get(i)
						.toString());
				Card questionCard = new Card(flashCard.optString("title"),
						flashCard.optString("front_img"),
						flashCard.optString("front_caption"));
				Card answerCard = new Card(flashCard.optString("title"),
						flashCard.optString("back_img"),
						flashCard.optString("back_caption"));
				questionCards.add(questionCard);
				answerCards.add(answerCard);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		displayCard();
	}

	public class ResponseReceiver extends BroadcastReceiver {
		public static final String CARD_LIST_RESPONSE = "com.njfarrell.mobile.intent.action.cardList";

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = new Bundle();
			b = intent.getExtras();
			if (b.containsKey(ContentService.RESPONSE_JSON)) {
				try {
					Log.d(TAG, b.getString(ContentService.RESPONSE_JSON));
					response = new JSONObject(
							b.getString(ContentService.RESPONSE_JSON));
					if (response.has("success")) {
						boolean success = (Boolean) response.get("success");
						if (success && api_do.equals("get_cards")) {
							generateFlashCard(response);
						} else if(api_do.equals("get_cards")){
	                        generateFlashCard(response);
	                    }
//					Toast.makeText(CardListActivity.this,
//							response.getString("message"), Toast.LENGTH_SHORT)
//							.show();
					} else {
						Toast.makeText(CardListActivity.this, "You need internet access in order to download the current flash card deck", Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// Dont handle exception
				}
			}
		}
	}

	private class Card {
		String title;
		String imageId;
		String description;

		private String getTitle() {
			return title;
		}

		private String getImageId() {
			return imageId;
		}

		private String getDescription() {
			return description;
		}

		private Card(String title, String imageId, String description) {
			this.title = title;
			this.imageId = imageId;
			this.description = description;
		}
	}
}
