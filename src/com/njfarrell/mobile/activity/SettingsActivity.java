package com.njfarrell.mobile.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.njfarrell.mobile.database.ModernLearningChemistryDB;
import com.njfarrell.mobile.database.ResponseTable;

public class SettingsActivity extends PreferenceActivity{
	//TODO setup a reverse mode preference option
	
	@Override
	public void onCreate(Bundle savedInstanceState) {    
	    super.onCreate(savedInstanceState);
	    addPreferencesFromResource(R.xml.settings_preferences);
        Preference refreshPreference = (Preference) findPreference("refresh_content");
        refreshPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SQLiteDatabase db;
                ModernLearningChemistryDB MLChemistryDB = new ModernLearningChemistryDB(
                        getApplicationContext());
                try {
                    db = MLChemistryDB.getReadableDatabase();
                } catch (SQLiteException e) {
                    return false;
                }
                String decrementQuery = "UPDATE "+ResponseTable.RESPONSE_TABLE_NAME+" SET "+ResponseTable.TIMESTAMP+" = '0';";
                if(db != null){
                    Cursor c = null;
                    try {
                        c = db.rawQuery(decrementQuery, null);
                        c.moveToFirst();
                    } catch (SQLiteException e) {
                        c = null;
                        db.close();
                    }
                }
                findPreference("refresh_content").setSummary("The content will be updated next time you have a data connection.");
                return false;
            }
        });
	}

}
