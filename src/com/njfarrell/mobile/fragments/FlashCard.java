package com.njfarrell.mobile.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.njfarrell.mobile.activity.CardListActivity;
import com.njfarrell.mobile.activity.R;
import com.njfarrell.mobile.utilities.ImageDownloader;

public class FlashCard extends Fragment
        implements View.OnClickListener{
	private static final String TAG = FlashCard.class.getSimpleName();

	private int pageNumber;
	private int maxNumber;
	private String title;
	private String imageId;
	private String description;
	
	private CardListActivity mActivity;
    private GestureDetector gestureDetector;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate:");

        mActivity = (CardListActivity) getActivity();

		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if(args != null){
            pageNumber = args.getInt("pageNumber");
			title = args.getString("title");
			imageId = args.getString("imageId");
			description = args.getString("description");
			maxNumber = args.getInt("maxNumber");
        }
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView flashcardTitle = (TextView) getActivity().findViewById(R.id.title);
        TextView flashcardNumber = (TextView) getActivity().findViewById(R.id.card_position);
        ImageView flashcardImage = (ImageView) getActivity().findViewById(R.id.card_image);
        TextView flashcardDesc = (TextView) getActivity().findViewById(R.id.description);
        View flashcard = (View) getActivity().findViewById(R.id.card_view);
        flashcard.setOnClickListener(this);
        
        gestureDetector = new GestureDetector(new GestureListener());
        flashcard.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
 
                if (gestureDetector.onTouchEvent(event)) {
                	Log.d(TAG, "Swipe event!");
                	return true;
                }
                return false;
            }
        });
        

        if(title != null){
            flashcardTitle.setText(title);
        }
        if(imageId != null){
        	ImageDownloader imageDownloader = new ImageDownloader();
			imageDownloader.download(imageId, flashcardImage);
        }
        if(description != null){
            flashcardDesc.setText(description);
        }
        flashcardNumber.setText("Card " + (pageNumber + 1) + " out of " + (maxNumber + 1));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
    	if (mActivity != null) {
    		mActivity.onFlashCardClick(pageNumber);
    	}
    }

    public class GestureListener extends SimpleOnGestureListener{
        
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;
       
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, 
                                        float velocityX, float velocityY) {
           if (mActivity != null) {
        	
	            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && 
	                         Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                //From Right to Left
	            	mActivity.onFlashCardSwipe("Right");
	                return true;
	            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
	                         Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                //From Left to Right
	            	mActivity.onFlashCardSwipe("Left");
	                return true;
	            }
           }
            return false;
        }
        @Override
        public boolean onDown(MotionEvent e) {
            //always return true since all gestures always begin with onDown and<br>
            //if this returns false, the framework won't try to pick up onFling for example.
            return false;
        }
    }
}

