package com.njfarrell.mobile.fragments;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.njfarrell.mobile.activity.CardListActivity;
import com.njfarrell.mobile.activity.R;
import com.njfarrell.mobile.utilities.ImageDownloader;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class CardDecksListFragment extends ListFragment implements
		OnItemClickListener {

	private static final String TAG = CardDecksListFragment.class
			.getSimpleName();

	private ListView cardDeckList;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		CardDeck deck = null;
		List<CardDeck> cardDeck = new ArrayList<CardDeck>();

		Bundle args = getArguments();
		if (args != null) {
			int i = 0;
			while (args.containsKey("deck" + i)) {
				deck = (CardDeck) args.getParcelable("deck" + i);
				i++;
				cardDeck.add(deck);
			}
		}

		cardDeckList = (ListView) getActivity().findViewById(
				R.id.flash_card_deck_list);
		cardDeckList.setAdapter(new CardDeckAdapter(getActivity()
				.getBaseContext(), R.layout.layout_list_row, cardDeck));
		cardDeckList.setOnItemClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long id) {
		Log.d(TAG, "id: " + view.getId());
		 Intent i = new Intent(getActivity(), CardListActivity.class);
		 i.putExtra("id", view.getId());
		 getActivity().startActivity(i);

	}

	@SuppressWarnings("rawtypes")
	private class CardDeckAdapter extends ArrayAdapter {

		private int resource;
		private LayoutInflater inflater;
		private Context context;

		public CardDeckAdapter(Context context, int resourceId, List objects) {

			super(context, resourceId, objects);
			resource = resourceId;
			inflater = LayoutInflater.from(context);
			this.context = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			/* create a new view of my layout and inflate it in the row */
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			convertView.findViewById(R.id.row_item).setBackgroundResource(
					R.drawable.listrow);
			convertView.setMinimumHeight(150);

			/* Extract the city's object to show */
			CardDeck cardDeck = (CardDeck) getItem(position);

			/* Take the TextView from layout and set the city's name */
			TextView txtName = (TextView) convertView
					.findViewById(R.id.cardDeckName);
			txtName.setText(cardDeck.getName());

            TextView txtDescription = (TextView) convertView
                    .findViewById(R.id.cardDeckDescription);
            txtDescription.setText(cardDeck.getDescription());

			/* Take the ImageView from layout and set the city's image */
			if (cardDeck.getImage() != null) {
				ImageView deckImage = (ImageView) convertView
						.findViewById(R.id.cardDeckImage);
				ImageDownloader imageDownloader = new ImageDownloader();
				imageDownloader.download(cardDeck.getImage(), deckImage);
			}
			convertView.setId(cardDeck.getDeckId());
			return convertView;
		}
	}

	public class CardDeck implements Parcelable{

		/**
		 * 
		 */
		private String name;
        private String description;
		private String image;
		private String deckGroup;
		private int deckId;

		public String getName() {
			return name;
		}

		// public void setName(String name) {
		// this.name = name;
		// }

		public String getImage() {
			return image;
		}

		// public void setImage(String image) {
		// this.image = image;
		// }

		public String getGroup() {
			return deckGroup;
		}

		public int getDeckId() {
			return deckId;
		}

        public String getDescription(){
            return description;
        }

		public CardDeck(String name, String description, String image, String group, int deckId) {
			super();
			this.name = name;
            this.description = description;
			this.image = image;
			this.deckGroup = group;
			this.deckId = deckId;
		}

		@Override
		public int describeContents() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			// TODO Auto-generated method stub
			
		}

	}
}
