package com.njfarrell.mobile.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.njfarrell.mobile.database.ModernLearningChemistryDB;
import com.njfarrell.mobile.database.ResponseTable;

public class ContentService extends IntentService {

	private static final String TAG = ContentService.class.getSimpleName();

	public static final String URL = "http://modernlearningtools.com/api.php?";
	
	private static final String CARD_DECK_RESPONSE = "com.njfarrell.mobile.intent.action.cardDeck";
	private static final String CARD_LIST_RESPONSE = "com.njfarrell.mobile.intent.action.cardList";

	private Bundle mBundle;

	private String api_do = null;
	private String api_for = null;
	private String api_token = null;
	private String api_with = null;
	private long maxAge = 0;
	private boolean cache = false;
	private String api_url = null;

	private String content = null;
    private String message = null;

	public static final String RESPONSE_JSON = "responseJson";

	public ContentService() {
        super("ContentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "ContentService Started");
		content = null;
		mBundle = intent.getExtras();
		if (mBundle != null && !mBundle.isEmpty()) {
			getServiceExtras();
			/*
			 * Check database for data
			 * if the data timestamp is greater than maxAge set the data to content
			 */
			if (cache) {
				Cursor c = dbQuery(true);
				if (c != null) {
					ContentValues rowValues = updateDB(c);
					if (rowValues != null) {
						content = getTableRowResponse(rowValues);
                        message = "Content successfully loaded from database.";
					}
					c.close();
				}
			}
			/*
			 * If content null, try to download content from online
			 */
			if (content == null) {
				if (isOnline()) {
					content = requestData();
					if (content != null) {
						//Store downloaded content into DB
						if (cache) {
							insertResponseData(content);
						}
					}
				}
			}
			/*
			 * If content still null, try to pull any data from database to use
			 */
			if (content == null) {
				Cursor c = dbQuery(false);
                if (c != null) {
                    ContentValues rowValues = updateDB(c);
                    if (rowValues != null) {
                        content = getTableRowResponse(rowValues);
                        message = "Content successfully loaded from database.";
                    }
                    c.close();
                }
			}
		}

		Intent broadcastIntent = new Intent();
		if(api_do.equals("get_cards")){
			broadcastIntent.setAction(CARD_LIST_RESPONSE);
		} else {
			broadcastIntent.setAction(CARD_DECK_RESPONSE);
		}
		if (content == null) {
			try {
				JSONObject response = new JSONObject();
				response.put("message", "Error getting data from server or database");
				content = response.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if(message != null){
            try {
                JSONObject response = new JSONObject();
                response.put("message", message);
                Object data = null;
                try{
                    data = new JSONObject(content);
                }catch(JSONException e){
                    try{
                    data = new JSONArray(content);
                    } catch(JSONException e1){
                        data = "";
                    }
                }
                response.put("data", data);
                response.put("success", true);
                content = response.toString();
            } catch (JSONException e){
                e.printStackTrace();
            }

        }
		broadcastIntent.putExtra(RESPONSE_JSON, content);
		sendBroadcast(broadcastIntent);
	}

	private void insertResponseData(String content) {
		ContentValues values = new ContentValues();
		JSONArray contentArray = new JSONArray();
		String timestamp = null;
		String data = null;
		try {
			JSONObject responseContent = new JSONObject(content);
			contentArray = responseContent.getJSONArray("data");
			timestamp = responseContent.optString("timestamp");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (contentArray != null) {
			data = contentArray.toString();
			values.put("response", data);
		}
		if (api_for != null)
			values.put("api_for", api_for);
		if (api_do != null)
			values.put("api_do", api_do);
		if (api_with != null)
			values.put("api_with", api_with);
		if (api_token != null)
			values.put("api_token", api_token);
		if (timestamp != null)
			values.put("timestamp", timestamp);

		ResponseTable responseTable = new ResponseTable(getApplicationContext());
		responseTable.insertRow(values);
	}

	private void getServiceExtras() {
		api_do = mBundle.getString("do");
		api_with = mBundle.getString("with");
		api_for = mBundle.getString("for");
		api_token = mBundle.getString("token");
        Calendar cal = Calendar.getInstance();
        long currentTime;
        currentTime = cal.getTimeInMillis() / 1000;
        if(mBundle.containsKey("age")){
        	Log.d(TAG, "age=" + mBundle.getLong("age"));
            maxAge = (int)currentTime - mBundle.getLong("age");
        }
		cache = mBundle.getBoolean("cache");
		api_url = mBundle.getString("url");
	}

	private String requestData() {
		String resultString = null;
		JSONObject resultData = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(api_url);
			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();

			if (api_do != null)
				postParameters.add(new BasicNameValuePair("do", api_do));
			if (api_with != null)
				postParameters.add(new BasicNameValuePair("with", api_with));
			if (api_for != null)
				postParameters.add(new BasicNameValuePair("for", api_for));
			if (api_token != null)
				postParameters.add(new BasicNameValuePair("token", api_token));

			httpPost.setEntity(new UrlEncodedFormEntity(postParameters));

			long t = System.currentTimeMillis();
			HttpResponse response = (HttpResponse) httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();
			InputStream instream = responseEntity.getContent();
			Log.i(TAG,
					"HTTPResponse received in ["
							+ (System.currentTimeMillis() - t) + "ms]");
			Log.d(TAG, "HTTPResponse: " + response.toString());
			resultString = convertStreamToString(instream);
			instream.close();
			try {
				resultData = new JSONObject(resultString);
				Log.d(TAG, resultData.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultString;
	}

	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm != null
				&& (cm.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED)
				|| (cm.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)) {
			return true;
		}
		return false;
	}

	private Cursor dbQuery(boolean checkTimeStamp) {
		SQLiteDatabase db;
		ModernLearningChemistryDB MLChemistryDB = new ModernLearningChemistryDB(
				getApplicationContext());
		try {
			db = MLChemistryDB.getReadableDatabase();
		} catch (SQLiteException e) {
			return null;
		}
		if (db != null) {
            if(!checkTimeStamp){
                maxAge = 0;
            }

            Log.d(TAG, "maxAge=" + maxAge);

            String sql = String.format("select * from %s where api_do = '%s' and timestamp >= %s",
                    ResponseTable.RESPONSE_TABLE_NAME,
                    api_do,
                    maxAge);
            if(api_with != null){
                sql += String.format(" and api_with='%s'", api_with);
            }
            sql += " ORDER BY timestamp DESC";
            Log.d("ContentService", "Query: "+sql);
			Cursor c = null;
			try {
                c = db.rawQuery(sql, null);
			} catch (SQLiteException e) {
				c = null;
                db.close();
			} finally {
				
			}
			return c;
		}
		return null;
	}

	private ContentValues updateDB(Cursor c) {
		ContentValues values = null;
		if (c.moveToFirst()) {
            values = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(c, values);
            Log.d(TAG, "values=" + values.toString());
		}
		return values;
	}

	private String getTableRowResponse(ContentValues values) {
		return values.getAsString(ResponseTable.RESPONSE);
	}
}
