package com.njfarrell.mobile.database;

import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class ModernLearningChemistryDB extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "Modern_Learning_Chemistry_DB";
	private static final int DATABASE_VERSION = 1;
	
    private static final String SELECT_FROM_TABLE_1 = "select * from %s limit 1";
	
	private Context mContext;
	
	public ModernLearningChemistryDB(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		mContext = context;
	}
	
	public ModernLearningChemistryDB(Context context, String name, int version) {
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		ResponseTable responseTable = new ResponseTable(mContext);
		responseTable.createTableIfNotExists(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
	
	/**
     * Check if the specified table is empty.
     *
     * @param table: the table to check
     * @return true: table exists
     */
    public synchronized boolean tableEmptyOrNotExists(String table) {
        boolean result = true;
        final String sql = String.format(SELECT_FROM_TABLE_1, table);        
        final SQLiteDatabase db = getReadableDatabase();
        Cursor cur = null;
        if (db != null) {
            try {
                cur = db.rawQuery(sql, null);
		        if (cur != null && cur.getCount() > 0) {
		            cur.moveToFirst(); // Always one row returned.
		            if (cur.getInt(0) > 0) { // Zero count means empty table.
		                result = false;
		            }
		            cur.close();
		        }
	        } catch (Exception e) {
		    	db.endTransaction();
		        db.close();
	        }
        }
        return result;
    }
	
	public void dropTableIfExists(String table){
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("drop table if exists " + table);
		db.close();
	}
	
	public synchronized long insertRow(String table, ContentValues values){
		long result = -1;
		SQLiteDatabase db = getWritableDatabase();
		try{
			result = db.insertOrThrow(table, null, values);
		} catch (SQLiteException e){
			db.endTransaction();
			db.close();
		}
		return result;
	}
}
