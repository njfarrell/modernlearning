package com.njfarrell.mobile.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class ResponseTable {
	public static final String RESPONSE_TABLE_NAME = "response_table";

	public static final String API_FOR = "api_for";
	public static final String API_DO = "api_do";
	public static final String API_WITH = "api_with";
	public static final String API_TOKEN = "api_token";
	public static final String RESPONSE = "response";
	public static final String TIMESTAMP = "timestamp";

	private ModernLearningChemistryDB mMLChemistryDB;

	public ResponseTable(Context context) {
		mMLChemistryDB = new ModernLearningChemistryDB(context);
	}

	public void createTableIfNotExists(SQLiteDatabase db) {
		String sql = String.format(
				"create table if not exists %s ("
						+ "id integer primary key autoincrement, "
						+ "%s TEXT, %s TEXT, %s TEXT, "
						+ "%s TEXT, %s TEXT, %s TEXT);", RESPONSE_TABLE_NAME,
				API_DO, API_WITH, API_FOR, API_TOKEN, RESPONSE, TIMESTAMP);
		db.execSQL(sql);
	}

	public long insertRow(ContentValues values) {
		return mMLChemistryDB.insertRow(RESPONSE_TABLE_NAME, values);
	}

}
